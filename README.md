# Nitrogen Configuration

Nitrogen configuration (fizzy-compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-nitrogen/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
